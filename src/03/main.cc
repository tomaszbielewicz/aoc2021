#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

int day3_part1(char* filename)
{
    std::string bits_str;
    std::ifstream input(filename);
    std::vector<int> bits;

    getline(input, bits_str);
    
    for(auto c : bits_str)
    {
    	if (c == '0')
	    	bits.push_back(-1);	
		else if (c == '1')
	    	bits.push_back(1);
    }


    while (getline(input, bits_str))
    {
    	int iter = 0;
		for(auto c : bits_str)
		{ 
	    	if (c == '0')
				bits.at(iter++) -= 1;
	    	else if (c == '1')
	    		bits.at(iter++) += 1;
		}
    }

    int gamma = 0, epsilon = 0;
    int base = 1;
    // Go in reverse order, from least significant bit
    for (auto rit = bits.rbegin(); rit != bits.rend(); ++rit)
    {
		if (*rit > 0) 
			gamma += base;  

		if (*rit < 0)
			epsilon += base;

		base *= 2;
    }

    return gamma * epsilon;
}

int day3_part2(char *filename)
{
    std::string bits_str;
    std::ifstream input(filename);
    std::vector<std::string> all_values;

    while (getline(input, bits_str)) 
	if (bits_str.size() != 0)
	    all_values.push_back(bits_str);

    // Lenght of the word is constant
    int positions_num = bits_str.size();
    std::vector<std::string> most_common_values;
    std::vector<std::string> least_common_values;

    int zeroes = 0, ones = 0;
    std::vector<std::string> zeroes_vec;
    std::vector<std::string> ones_vec;
    for (auto val : all_values)
    {
    	if (val.at(0) == '0') 
		{
			zeroes++;
			zeroes_vec.push_back(val);
		}
		else if (val.at(0) == '1') 
		{
			ones++;	
			ones_vec.push_back(val);
		}
    }

    most_common_values = ones > zeroes 
    	? ones_vec 
    	: zeroes_vec;

    least_common_values = ones > zeroes 
    	? zeroes_vec 
    	: ones_vec;

    int pos = 0;
    while (most_common_values.size() > 1)
    {
		zeroes_vec.clear();
		ones_vec.clear();
		zeroes = 0;
		ones = 0;
		pos++;
		for (auto val : most_common_values)
		{
			if (val.at(pos) == '0') 
			{
			zeroes++;
			zeroes_vec.push_back(val);
			}
			if (val.at(pos) == '1') 
			{
			ones++;	
			ones_vec.push_back(val);
			}
		}

		most_common_values = ones > zeroes 
			? ones_vec 
			: (ones == zeroes ? ones_vec : zeroes_vec);
    }

    pos = 0;
    while (least_common_values.size() > 1)
    {
		zeroes_vec.clear();
		ones_vec.clear();
		zeroes = 0;
		ones = 0;
		pos++;
		for (auto val : least_common_values)
		{
			if (val.at(pos) == '0') 
			{
			zeroes++;
			zeroes_vec.push_back(val);
			}
			if (val.at(pos) == '1') 
			{
			ones++;	
			ones_vec.push_back(val);
			}
		}

		least_common_values = ones > zeroes 
			? zeroes_vec 
			: (ones == zeroes ? zeroes_vec : ones_vec);
    }

    std::string oxy_gen_rating_str = most_common_values.at(0);
    std::string co2_scrub_rating_str = least_common_values.at(0);
    int oxy_gen_rating = 0;
    int co2_scrub_rating = 0;
    int base = 1;
    // Go in reverse order, from least significant bit
    for (auto rit = oxy_gen_rating_str.rbegin(); rit != oxy_gen_rating_str.rend(); ++rit)
    {
		if (*rit == '1') 
	    	oxy_gen_rating += base;

		base *= 2;
    }

    base = 1;
    for (auto rit = co2_scrub_rating_str.rbegin(); rit != co2_scrub_rating_str.rend(); ++rit)
    	{
		if (*rit == '1') 
	    	co2_scrub_rating += base;

		base *= 2;
    }

    return oxy_gen_rating * co2_scrub_rating;
}

int main(int argc, char* args[])
{
    std::cout << "Advent of Code 2021, challenges answers:" << std::endl;
    std::cout << "Day 3, Part 1 answer: " << day3_part1(args[1]) << std::endl;
    std::cout << "Day 3, Part 2 answer: " << day3_part2(args[1]) << std::endl;
    return 0;
}

