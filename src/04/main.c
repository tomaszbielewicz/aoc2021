#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#if 0
#define INPUT_FILE "./sample.txt"
#else
#define INPUT_FILE "./input.txt"
#endif

typedef struct {
    int *board_nr_arr, *row_arr, *col_arr; // The same value can be at multiple places
    bool *marked_arr;
    int val;
    size_t used;
    size_t size;
} BoardSpace;

void init_BoardSpace(BoardSpace *bs) 
{
    bs->row_arr = malloc(sizeof(int));
    bs->col_arr = malloc(sizeof(int));
    bs->marked_arr = malloc(sizeof(bool));
    bs->board_nr_arr = malloc(sizeof(int));
    bs->used = 0;
    bs->size = 1;
    bs->val = 0;
}

int main()
{
    FILE *fp;
    char str_buff[1024];
    
    fp = fopen(INPUT_FILE, "r");
    fscanf(fp, "%s", str_buff);
    // Reading drawn numbers
    // ------------------------------------------------------------
    char r_number[2];             
    int drawn_numbers[1024];
    int drawn_numbers_l = 0;
    
    char r_char = '0';
    for (int i = 0, position = 0; r_char != '\0'; i++) {
        r_char = str_buff[i];   
        // Char is numeric
        if (r_char >= '0' && r_char <= '9') {
            r_number[position++] = r_char;   
        } else if (r_char == ',') {        
            // Convert numeric string to integer
            drawn_numbers[drawn_numbers_l++] = atoi(r_number);
            position = 0;
            memset(r_number, ' ', 2);
        }
    } 

    // Don't forget about the last number!
    drawn_numbers[drawn_numbers_l++] = atoi(r_number);
    
    // Reading bingo boards
    // ------------------------------------------------------------ 

    BoardSpace board_spaces[100];
    BoardSpace* boards_structure[1000][5][5];
    const int NUMS_IN_ROW = 25;
    int r_board_number[5][5];
    int board_nr = 0;

    for (int i = 0; i < 100; i++) {    
        init_BoardSpace(&board_spaces[i]);
    }

    while (!feof(fp))
    {   
        fscanf(fp, "%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d\n", 
            &r_board_number[0][0], 
            &r_board_number[0][1], 
            &r_board_number[0][2], 
            &r_board_number[0][3], 
            &r_board_number[0][4],
            &r_board_number[1][0], 
            &r_board_number[1][1], 
            &r_board_number[1][2], 
            &r_board_number[1][3], 
            &r_board_number[1][4],
            &r_board_number[2][0], 
            &r_board_number[2][1], 
            &r_board_number[2][2], 
            &r_board_number[2][3], 
            &r_board_number[2][4],
            &r_board_number[3][0], 
            &r_board_number[3][1], 
            &r_board_number[3][2], 
            &r_board_number[3][3], 
            &r_board_number[3][4],
            &r_board_number[4][0], 
            &r_board_number[4][1], 
            &r_board_number[4][2], 
            &r_board_number[4][3], 
            &r_board_number[4][4]
            );

        // Update coords in mapping structure
        for (int row = 0, idx = 0; row < 5; row++) 
            for (int col = 0; col < 5; col++)
            {
                idx = r_board_number[row][col];
                
                if (board_spaces[idx].used == board_spaces[idx].size) 
                {
                    board_spaces[idx].size *= 2;
                    board_spaces[idx].col_arr = realloc(board_spaces[idx].col_arr, board_spaces[idx].size * sizeof(int));
                    board_spaces[idx].row_arr = realloc(board_spaces[idx].row_arr, board_spaces[idx].size * sizeof(int));
                    board_spaces[idx].marked_arr = realloc(board_spaces[idx].marked_arr, board_spaces[idx].size * sizeof(bool));
                    board_spaces[idx].board_nr_arr = realloc(board_spaces[idx].board_nr_arr, board_spaces[idx].size * sizeof(int));
                }

                board_spaces[idx].col_arr[board_spaces[idx].used] = col;
                board_spaces[idx].row_arr[board_spaces[idx].used] = row;
                board_spaces[idx].marked_arr[board_spaces[idx].used] = false;
                board_spaces[idx].board_nr_arr[board_spaces[idx].used] = board_nr;
                board_spaces[idx].val = idx; // todo: potrzebne?
                board_spaces[idx].used++;

                boards_structure[board_nr][row][col] = &board_spaces[idx]; 
            }

        board_nr++;
    }

    for (int i = 0; i < drawn_numbers_l; i++) 
    {
        int drawn_number = drawn_numbers[i];
        int num_of_places = board_spaces[drawn_number].used;

        for (int j = 0; j < num_of_places; j++) 
        {
            int board_nr = board_spaces[drawn_number].board_nr_arr[j];
            int row = board_spaces[drawn_number].row_arr[j];
            int col = board_spaces[drawn_number].col_arr[j];
            board_spaces[drawn_number].marked_arr[j] = true;

            int win_r = 1;
            for (int x = 0; x < 5; x++) {
                if (boards_structure[board_nr][x][col]->marked_arr[j] == false) 
                    win_r = 0;
            }
   
            int win_c = 1;
            for (int x = 0; x < 5; x++) {
                if (boards_structure[board_nr][row][x]->marked_arr[j] == false) 
                    win_c = 0;
            }

            if (win_c > 0 || win_r)
            {
                printf ("\nyou won with %d on board %d!\n", drawn_number, board_nr);
                int sum = 0;
                for (int ii = 0; ii < 5; ii++) {
                    for (int jj = 0; jj < 5; jj++) {
                        if (boards_structure[board_nr][ii][jj]->marked_arr[j] == false) 
                            sum += boards_structure[board_nr][ii][jj]->val;
                    }                
                }

                printf ("result: %d", sum * drawn_number);
                return 0;
            }

        }
    
    }

    fclose(fp);
    return 0;
}

