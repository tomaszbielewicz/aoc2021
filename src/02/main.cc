#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

int part1(char* filename)
{
    std::string operation;
    std::string value;
    std::ifstream input(filename);

    int horiz = 0, depth = 0;

    while (getline(input, operation, ' '))
    {
    	if (operation.empty())
	    break;

	getline(input, value);

	if (value.empty())
	    break;

	if (operation.compare("forward") == 0)
	    horiz += std::stoi(value);	
	
	if (operation.compare("down") == 0)
	    depth += std::stoi(value);
	
	if (operation.compare("up") == 0)
	    depth -= std::stoi(value);
    }

    return horiz * depth; 
}

int part2(char* filename)
{
    std::string operation;
    std::string value_str;
    std::ifstream input(filename);

    int horiz = 0, depth = 0, aim = 0;

    while (getline(input, operation, ' ')) 
    {
    	if (operation.empty())
    	    break;

    	getline(input, value_str);

    	if (value_str.empty())
    	    break;

	if (operation.compare("forward") == 0)
	{
	    int value = std::stoi(value_str);
	    horiz += value;
	    depth += aim * value;
	}
	
	if (operation.compare("down") == 0)
	{
	    int value = std::stoi(value_str);
	    aim += value;
	}
	
	if (operation.compare("up") == 0)
	{
	    int value = std::stoi(value_str);
	    aim -= value;
	}
    }

    return horiz * depth;
}

int main(int argc, char* args[])
{
    std::cout << "Advent of Code 2021, challenges answers:" << std::endl;
    std::cout << "Day 2, Part 1 answer: " << part1(args[1]) << std::endl;
    std::cout << "Day 2, Part 2 answer: " << part2(args[1]) << std::endl;
    return 0;
}

